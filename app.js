const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Direccion de la ciudad',
        demand: true
    }
}).argv;


const getInfo = async(dire) => {

    try {
        const cordenadas = await lugar.getLugarLatLng(dire);
        const temp = await clima.getClima(cordenadas.lat, cordenadas.lng);
        return `El clima de ${dire} es de ${temp}`;
    } catch (error) {
        return `No se pudo determinar el clima de ${dire}`;
    }

};

getInfo(argv.direccion)
    .then(console.log)
    .catch(console.log);

// lugar.getLugarLatLng(argv.direccion)
//     .then(console.log);

// clima.getClima(40.750000, -74.000000)
//     .then(console.log)
//     .catch(e => {
//         console.log(e)
//     });